import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * Clase que tiene todos los metodos que se realizaran con los polinomios
 * @author Garrido Valencia Alan
 * @author Lopez Coria Pedro
 * @author Segura Gravito Jorge
 * @author Hernandez Diego
 */
public class Polinomio {
    //lista que tiene los terminos
    public ArrayList<Termino> terminos = new ArrayList<Termino>();
    
    
    //agregar terminos al polinomio
    public void insrPolinomioTEMP(){
        System.out.println("Escriba su polinomio: "); 
        String dato = Test.teclado.nextLine();
        System.out.println();
        StringTokenizer st = new StringTokenizer( dato, "+-",true);
        int nTokens = st.countTokens();
        boolean inicio= true;
        double [] arreglo = new double[20];
        char signo='+';
        while ( st.hasMoreTokens() ) {
            String val = st.nextToken();
            if(val.length()==1 && !(Character.isDigit(val.charAt(0)))){
                signo=val.charAt(0);
            }else{
            int aux1 = val.indexOf("x^");
            int aux2 = val.indexOf(String.valueOf(val));
            
            if (aux1 != - 1)
            {
                String izq, der;
                izq = val.substring(0,aux1);
                der = val.substring(aux1+2);
               
                if (inicio)
                {
                    if(signo=='-'){
                        arreglo = new double [Integer.valueOf(der) + 1];
                        arreglo[ Integer.valueOf(der) ] = (-1)*Integer.valueOf(izq);
                        inicio = false;    
                    }else{
                        arreglo = new double [Integer.valueOf(der) + 1];
                        arreglo[ Integer.valueOf(der) ] = Integer.valueOf(izq);
                        inicio = false;
                    }
                }
                else
                {
                    if(signo=='-'){
                        arreglo[ Integer.valueOf(der) ] = (-1)*Double.valueOf(izq);
                    }else{
                        arreglo[ Integer.valueOf(der) ] = Double.valueOf(izq);    
                    }
                }
            }
            else{
                if(signo=='-'){
                    arreglo[0] = (-1)*Double.valueOf(val);
                }else{
                    arreglo[0] = Double.valueOf(val);    
                }
            }
            }
        }
        for (int i=0; i < arreglo.length; i++)
            this.terminos.add(new Termino(arreglo[i],i));
    }
    
    //constructor vacio
    public Polinomio(){
        this.terminos.clear();
    }
    
    //Impresión de polinomio
    public void impPolinomio(){
        System.out.print(" (");
        for(Termino ter : terminos){
            ter.impTermino();
        }
        System.out.print(")");
        System.out.println("");
    }
    
    //Operaciones de polinomios
    //suma
    public Polinomio sumaPolinomio(Polinomio arg){
        Polinomio res = new Polinomio();
        Polinomio p1 = this;
        Polinomio p2 = arg;
        
        //En caso de que los polinomios sean de distinto tamaño, se agregan terminos vacios
        if(p1.terminos.size() != p2.terminos.size()){
            int i;
            if(p1.terminos.size() > p2.terminos.size()){
                i = p2.terminos.size(); 
                while(p1.terminos.size() > p2.terminos.size()){
                    p2.terminos.add(new Termino(0,p2.terminos.size()));    
                }
            }
            else{
                i = p1.terminos.size(); 
                while(p2.terminos.size() > p1.terminos.size()){
                    p1.terminos.add(new Termino(0,p1.terminos.size()));    
                }
            }
        }
        
        //hace la suma de cada uno de los terminos
        for( int j = 0 ; j < p2.terminos.size() ; j++ ){
            res.agregar(p1.terminos.get(j).sumaTerm(p2.terminos.get(j)));
        }
        return res;
        
    }
    
    public Polinomio restaPolinomio(Polinomio arg){
        Polinomio res = new Polinomio();
        Polinomio p1 = this;
        Polinomio p2 = arg;
        
        //En caso de que los polinomios sean de distinto tamaño, se agregan terminos vacios
        if(p1.terminos.size() != p2.terminos.size()){
            int i;
            if(p1.terminos.size() > p2.terminos.size()){
                i = p2.terminos.size(); 
                while(p1.terminos.size() > p2.terminos.size()){
                    p2.terminos.add(new Termino(0,p2.terminos.size()));    
                }
            }
            else{
                i = p1.terminos.size(); 
                while(p2.terminos.size() > p1.terminos.size()){
                    p1.terminos.add(new Termino(0,p1.terminos.size()));    
                }
            }
        }
        
        //hace la suma de cada uno de los terminos
        for( int j = 0 ; j < p2.terminos.size() ; j++ ){
            res.agregar(p1.terminos.get(j).restTerm(p2.terminos.get(j)));
        }
        return res;
        
    }
    
    public Polinomio multiPolinomio(Polinomio arg){
        Polinomio res = new Polinomio();
        Polinomio p1 = this;
        Polinomio p2 = arg;
        
        //para esta operacion se establece el tamaño de la lista del resultado
        
        //El grado del polinomio resultado es igual a la suma de los grados de los polinomios que se multiplican
        for(int i = 0; i < (p1.terminos.size() + p2.terminos.size()) ; i++)
            res.agregar(new Termino(0,i)); //Un elemento vacio
        
        //Ahora se hace la multiplicacion de termino a termino y todo se va sumando al polinomio resultado
        Termino temp = new Termino();
        //Un for anidado que recorre todas las multiplicaciones
        for(int j = 0; j < p1.terminos.size() ; j++){
            for(int i = 0; i < p2.terminos.size() ; i++){
                
                temp = p1.terminos.get(j).multTerm(p2.terminos.get(i));
                temp = res.terminos.get(temp.grado).sumaTerm(temp);
                res.terminos.set(temp.grado, temp);
                
            }
        }
        
        return res;
        
    }
    
    public Polinomio opuestoPolinomio(){
        
        Polinomio opuesto = new Polinomio();
        for (Termino ter : this.terminos){
            opuesto.agregar(ter.opuestoTerm(ter));
        }
        
        return opuesto;
    }
    
    public double valuarPolinomio(double num){
        
        double numero=0;
        for(Termino ter : this.terminos){
            numero += ter.evaluaTermino(ter, num);
        }
        
        return numero;
    }
    
    public boolean igualdadPolinomio(Polinomio poli){
        
        boolean bandera=true;
        Polinomio pol = poli;
        try{
            for (Termino term : this.terminos){
                bandera = term.iguaTerm(pol.terminos.remove(0));
            }
        }catch(IndexOutOfBoundsException e){ return false;}
        
        return bandera;
    }

    

    //Manejo de arraylist de polinomio

    //Agregar un termino a la lista
    public void agregar(Termino ter){
        terminos.add(ter);
    }
    
    //Quitar un elemento
    public void remover(Termino ter){
        terminos.remove(ter);
    }
    
    public void vaciar(){
        terminos.clear();
    }
}
