import java.util.Scanner;

/**
 * Clase principal para ejecutar el programa
 * @author Garrido Valencia Alan
 * @author Lopez Coria Pedro
 * @author Segura Gravito Jorge
 * @author Hernandez Diego
 */
public class Test {
    public static Scanner teclado = new Scanner(System.in);
    //int i = sc.nextInt();
    
    
    public static void main(String[] args) {
        Scanner seleccion = new Scanner(System.in);
        int operacion;
        char res;

        System.out.println("A continuación introduce el primero polinomio: ");
        Polinomio p1 = new Polinomio();
        p1.insrPolinomioTEMP();
        System.out.print("El polinomio introducido es: ");
        p1.impPolinomio();
        System.out.println("\nA continuación introduce el segundo polinomio: ");
        Polinomio p2 = new Polinomio();
        p2.insrPolinomioTEMP();
        System.out.print("El polinomio introducido es: ");
        p2.impPolinomio();
        do{
            System.out.println("\nSelecciona la operación que desea realizar: ");
            System.out.println("1. Suma");
            System.out.println("2. Resta");
            System.out.println("3. Multiplicacion");
            System.out.println("4. Polinomio opuesto");
            System.out.println("5. Valor en un punto");
            System.out.println("6. Igualdad de polinomios");
    
                operacion = seleccion.nextInt();
    
                switch (operacion) {
                    case 1: Polinomio p3 = new Polinomio();
                            p3 = p1.sumaPolinomio(p2);
                            p3.impPolinomio();
                            p3=NULL;
                            break;
                        
                    case 2: Polinomio p4 = new Polinomio();
                            p4 = p1.restaPolinomio(p2);
                            p4.impPolinomio();
                            p4=NULL;
                            break;
                        
                    case 3: Polinomio p5 = new Polinomio();
                            p5 = p1.multiPolinomio(p2);
                            p5.impPolinomio();
                            p5=NULL;
                            break;
                        
                    case 4: Polinomio p6 = new Polinomio();
                            System.out.println("Primer polinimio\n");
                            p6 = p1.opuestoPolinomio();
                            p6.impPolinomio();
                            System.out.println("\nSegundo Polinomio\n");
                            Polinomio p8 = new Polinomio();
                            p8 = p2.opuestoPolinomio();
                            p8.impPolinomio();
                            p6=NULL;
                            p7=NULL;
                            p8=NULL;
                            break;
                        
                    case 5: System.out.println("Dame el valor del punto a evaluar en el primer polinomio: ");
                            int valor= seleccion.nextInt();
                            double valor1 = p1.valuarPolinomio(valor);
                            System.out.println("Dame el valor del punto a evaluar en el segundo polinomio: ");
                            int valor2= seleccion.nextInt();
                            double valor3 = p2.valuarPolinomio(valor2);
                            System.out.println("Del primer polinomio: x= " + valor1 + "\nDel segundo polinomio: x= " + valor3);
                            valor=NULL;
                            valor2=NULL;
                            valor3=NULL;
                            break;
                        
                    case 6: if(p1.igualdadPolinomio(p2))
                                System.out.println("Los polinomios son iguales");
                            else
                                System.out.println("Los polinomios no son iguales");
                            break;
                        
                    default: System.out.println("Opcion invalida");
                            break;
            }
            System.out.println("¿Desea realizar otra operacion? Y/N");
            res = seleccion.next().charAt(0);
        }while(res=='Y');
    }
}
