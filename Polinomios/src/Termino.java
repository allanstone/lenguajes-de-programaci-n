/**
 * Clase base que conformara los elementos del polinomio
 * @author Garrido Valencia Alan
 * @author Lopez Coria Pedro
 * @author Segura Gravito Jorge
 * @author Hernandez Diego
 */
public class Termino {

    public double coeficiente; //Abarca números reales
    public int grado; //Grado 0: x^0, Grado 1: x^1
    public char signo;
    public Termino() {
    }
    
    public Termino(double coef, int grad) {
        this.coeficiente = coef;
        this.grado = grad;
        if(this.coeficiente >= 0)
        	this.signo = '+';
        else
        	this.signo = '-';
    }

    public Termino sumaTerm(Termino arg) {
        return new Termino(this.coeficiente + arg.coeficiente, this.grado);
    }
    
    public Termino restTerm(Termino arg) {
        return new Termino(this.coeficiente - arg.coeficiente, this.grado);
    }

    public boolean iguaTerm(Termino arg) {
        if(this.grado == arg.grado && this.coeficiente == arg.coeficiente)
            return true;
        else
            return false;
    }
    
    public Termino multTerm(Termino arg) {
        return new Termino(this.coeficiente * arg.coeficiente, this.grado + arg.grado);
    }
    
    public Termino opuestoTerm(Termino arg){
    
    	if(arg.signo == '+')
    		arg.signo = '-';
    	else
    		arg.signo = '+';
    
            if (this.grado == 0) {
                System.out.print(this.signo + "" + Math.abs(this.coeficiente) + " ");
            } else if (this.grado == 1) {
                System.out.print(this.signo + "" + Math.abs(this.coeficiente) + "x" + " ");
            } else {
                System.out.print(this.signo + "" + Math.abs(this.coeficiente) + "x^" + this.grado + " ");
            }
            
            return arg;
        }
    //retorna el valor evaluado, donde se mande a llamar, que se vallan sumando
    //o restando dependiendo el signo
    public double evaluaTermino(Termino arg, double numero){
    	
    	return (Math.pow(numero,arg.grado) * arg.coeficiente);
    }
    

    public void impTermino() {

            if (this.grado == 0){
                System.out.print(this.signo + "" + Math.abs(this.coeficiente) + " ");
            } else if (this.grado == 1) {
                System.out.print(this.signo + "" + Math.abs(this.coeficiente) + "x" + " ");
            } else {
                System.out.print(this.signo + "" + Math.abs(this.coeficiente) + "x^" + this.grado + " ");
            }
        }
        
}
